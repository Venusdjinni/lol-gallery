import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:lolgallery/screens/search.dart';
import 'package:lolgallery/screens/tag_infos.dart';
import 'package:lolgallery/widgets/video_item.dart';

class DiscoverScreen extends StatefulWidget {
  final GlobalKey<NavigatorState> navKey;

  DiscoverScreen(this.navKey);

  @override
  _DiscoverScreenState createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {
  void search() {
    Navigator.of(context)
        .push(MaterialPageRoute(
          builder: (_) => Search(),
          fullscreenDialog: true
    ));
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Scaffold(
      appBar: AppBar(
        title: Text('Discover'),
        actions: <Widget>[
          IconButton(
            onPressed: search,
            icon: Icon(Icons.search),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 8.0,),
          Flexible(
            child: Swiper(
              itemBuilder: (context, index) {
                return new VideoItem();
              },
              itemCount: 10,
              viewportFraction: 0.8,
              scale: 0.9,
              //loop: false,
              onIndexChanged: (int) {},
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 8.0,
              left: 16.0,
              right: 16.0
            ),
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: Theme.of(context).accentColor
            ),
            height: 60.0,
            width: double.infinity,
            child: Text(
              "La description de la vidéo",
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(height: 8.0,),
        ],
      ),
    );

    return Navigator(
      key: widget.navKey,
      onGenerateRoute: (rs) =>
          MaterialPageRoute(
              builder: (context) => body
          ),
    );
  }
}
