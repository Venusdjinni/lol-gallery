import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:lolgallery/consts.dart';
import 'package:lolgallery/datamanager.dart';
import 'package:lolgallery/modele/dataformat.dart';
import 'package:lolgallery/widgets/tag_item.dart';
import 'package:video_player/video_player.dart';

class EditVideo extends StatefulWidget {
  final File video;

  const EditVideo({Key key, @required this.video}) : super(key: key);

  @override
  _EditVideoState createState() => _EditVideoState();
}

class _EditVideoState extends State<EditVideo> {
  VideoPlayerController _controller;
  List<Tag> tags = [];

  @override
  void initState() {
    super.initState();
    //SystemChrome.setEnabledSystemUIOverlays([]);

    _controller = VideoPlayerController
        .file(widget.video)
        //.network('http://46.101.90.180:8080/lolgallery/videos/b2072da4-641b-48ac-9c4d-cae08116abc0')
        ..setLooping(true)
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          setState(() {});
        });
  }

  @override
  Widget build(BuildContext context) {
    int size = widget.video.lengthSync();
    int duration = _controller.value.duration.inSeconds;

    Widget editTagsButton = FlatButton(
        onPressed: editTags,
        child: DecoratedBox(
          decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.white,
                  width: 2.0
              ),
              shape: BoxShape.circle
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              "${tags.length}",
              style: Theme.of(context).textTheme.body2.copyWith(
                  color: Colors.white,
                  fontSize: 16
              ),
            ),
          ),
        )
    );

    Widget addTagButton = FlatButton(
      onPressed: addTag,
      textColor: Colors.white,
      child: Text("Tags"),
    );

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Align(
          alignment: Alignment(0, 0.1),
          child: StatefulBuilder(
            builder: (_, s) {
              return Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      s(() {
                        onVideoTap();
                      });
                    },
                    child: AspectRatio(
                      aspectRatio: _controller.value.aspectRatio,
                      child: VideoPlayer(_controller),
                    ),
                  ),
                  IgnorePointer(
                    child: _controller.value.isPlaying ?
                    SizedBox() :
                    CircleAvatar(
                      radius: 32,
                      backgroundColor: Theme.of(context).primaryColorLight.withOpacity(0.5),
                      child: Icon(Icons.play_arrow, size: 48,),
                    ),
                  )
                ],
              );
            },
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          //resizeToAvoidBottomInset: true,
          appBar: AppBar(
            title: Text(
              "Publish video",
              style: TextStyle(
                color: Colors.white
              ),
            ),
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(
                color: Theme.of(context).backgroundColor
            ),
            actions: tags.isEmpty ? [addTagButton] : [editTagsButton, addTagButton],
          ),
          body: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              _Hole(),
              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.bottomCenter,
                      color: Colors.black.withOpacity(0.3),
                      padding: EdgeInsets.only(
                        left: 16.0,
                        right: 84.0
                      ),
                      child: TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Description de la video...",
                          hintStyle: TextStyle(
                            color: Colors.grey
                          )
                        ),
                        maxLength: 280,
                        maxLengthEnforced: false,
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor.withOpacity(0.6),
                            borderRadius: BorderRadius.circular(8.0)
                        ),
                        margin: EdgeInsets.all(8.0),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 6.0,
                            vertical: 2.0
                        ),
                        child: Text(
                          durationToString(duration),
                          style: TextStyle(
                              color: Theme.of(context).primaryColor
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor.withOpacity(0.6),
                            borderRadius: BorderRadius.circular(8.0)
                        ),
                        margin: EdgeInsets.all(8.0),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 6.0,
                            vertical: 2.0
                        ),
                        child: Text(
                          sizeToString(size),
                          style: TextStyle(
                              color: Theme.of(context).primaryColor
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: EdgeInsets.all(32.0),
                  child: FloatingActionButton(
                    onPressed: checkAndSave,
                    child: Icon(Icons.done_all),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    _controller.dispose();
  }

  void checkAndSave() {
  }

  void addTag() {
    Navigator.of(context)
        .push(MaterialPageRoute(
          builder: (_) => AddTagDialog(),
          fullscreenDialog: true
    ));
  }

  List<Widget> tagsWidgets = [];
  void editTags() async {
    tagsWidgets = tags.map(
      (t) => TagItem(
        t,
        onTap: () => _removeTag(context, t),
      )
    ).toList();

    await showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text("Added tags"),
          content: Container(
            height: MediaQuery.of(context).size.height * 0.6,
            width: 300,
            child: AnimatedList(
              initialItemCount: 10,
              itemBuilder: (_, i, a) =>
                SlideTransition(
                  position: a.drive(Tween<Offset>(
                    // todo: is it the needed value
                    begin: Offset(200, 0),
                    end: Offset.zero
                  )),
                  child: tagsWidgets[i],
                ),
            )
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.pop(context),
              child: Text("Cancel"),
            )
          ],
        );
      }
    );

    // update de la page
    setState(() {});
  }

  void _removeTag(BuildContext context, Tag tag) {
    AnimatedList.of(context).removeItem(
     tags.indexOf(tag),
      (_, a) => tagsWidgets[tags.indexOf(tag)]
    );
    tags.remove(tag);
  }

  void onVideoTap() {
    _controller.value.isPlaying ? _controller.pause() : _controller.play();
  }
}

class _Hole extends SingleChildRenderObjectWidget {
  @override
  RenderObject createRenderObject(BuildContext context) {
    // TODO: implement createRenderObject
    return new _HoleRenderObject();
  }

}

class _HoleRenderObject extends RenderBox {
  @override
  bool get sizedByParent => true;
}

class _Sieve extends SingleChildRenderObjectWidget {

  _Sieve({@required Widget child}) : super(child: child);

  @override
  RenderObject createRenderObject(BuildContext context) {
    // TODO: implement createRenderObject
    return new _SieveRenderObject();
  }
}

class _SieveRenderObject extends RenderBox with RenderObjectWithChildMixin {
  @override
  bool get sizedByParent => true;
}

class AddTagDialog extends StatefulWidget {
  @override
  _AddTagDialogState createState() => _AddTagDialogState();
}

class _AddTagDialogState extends State<AddTagDialog> {
  final TextEditingController controller = new TextEditingController();

  final StreamController<List<Tag>> _editedTagStream = StreamController();

  void _textChanged(String value) {
    DataManager.get().searchTags(value).then(
      (r) {
        r.add(Tag.fromJson({
          "tag" : value
        }));
        _editedTagStream.add(r);
      }
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: TextField(
          controller: controller,
          onChanged: _textChanged,
          decoration: InputDecoration(
              border: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  borderSide: BorderSide.none
              ),
              fillColor: Theme.of(context).accentColor,
              filled: true,
              contentPadding: EdgeInsets.symmetric(
                  horizontal: 8.0,
                  vertical: 8.0
              ),
            hintText: "enter tag..."
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            StreamBuilder<List<Tag>>(
              stream: _editedTagStream.stream,
              builder: (_, snapshot) {
                List<Tag> data = [];
                if (snapshot.hasData)
                  data = snapshot.data;

                return Wrap(
                  spacing: 4.0,
                  children: data.map((t) =>
                    TagItem(
                      t,
                      onTap: () => _onTagPressed(t),)
                  ).toList(),
                );
              },
            ),
            Divider(color: Theme.of(context).primaryColorDark,),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                "Featured",
                style: Theme.of(context).textTheme.subhead,
              ),
            ),
            FutureBuilder<List<Tag>>(
              future: DataManager.get().featuredTags(),
              builder: (_, snapshot) {
                List<Tag> data = [];
                if (snapshot.hasData)
                  data = snapshot.data;

                return Wrap(
                  spacing: 4.0,
                  children: data.map((t) =>
                    TagItem(
                      t,
                      onTap: () => _onTagPressed(t),)
                  ).toList(),
                );
              },
            ),
            Divider(color: Theme.of(context).primaryColorDark,),
          ],
        ),
      ),
    );
  }

  void _onTagPressed(Tag tag) {
    Navigator.pop(context, tag);
  }

  @override
  void dispose() {
    _editedTagStream.close();
    super.dispose();
  }


}


