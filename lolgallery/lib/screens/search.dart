import 'package:flutter/material.dart';
import 'package:lolgallery/widgets/video_mini_list.dart';

class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          decoration: InputDecoration(
            border: UnderlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: BorderSide.none
            ),
            fillColor: Theme.of(context).accentColor,
            filled: true,
            contentPadding: EdgeInsets.symmetric(
              horizontal: 8.0,
              vertical: 8.0
            ),
            suffix: GestureDetector(
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text("Search"),
              ),
            )
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              ActionChip(label: Text("By video"), onPressed: () {},),
              ActionChip(label: Text("By tag"), onPressed: () {},),
              ActionChip(label: Text("By user"), onPressed: () {},),
            ],
          ),
          Expanded(
            child: VideoMiniList(),
          )
        ],
      ),
    );
  }
}
