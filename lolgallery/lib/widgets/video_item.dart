import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/screens/viewmedia.dart';

class VideoItem extends StatelessWidget {
  void _onTap(BuildContext context) {
    Navigator.of(context, rootNavigator: true).push(
        new MaterialPageRoute(
          builder: (_) => ViewMediaScreen(),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _onTap(context),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey.shade800,
          borderRadius: BorderRadius.circular(8)
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            SizedBox(
              child: Image.network(
                "https://i.pinimg.com/originals/78/e3/e3/78e3e3ec1e0bba2004a8fb1c87db47df.jpg",
                fit: BoxFit.fill,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.2/*.3*/),
                borderRadius: BorderRadius.circular(8.0)
              ),
              height: double.infinity,
              width: double.infinity,
              child: Icon(
                Icons.play_circle_outline,
                color: Colors.white,
                size: 60,
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                color: Colors.grey.shade200.withOpacity(0.7),
                height: 50.0,
                width: double.infinity,
                alignment: Alignment.bottomLeft,
                padding: EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 4.0
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text("by Emily Thorne"),
                    SizedBox(height: 4,),
                    Row(
                      children: <Widget>[
                        Expanded(child: Text("2 hours ago")),
                        Text("00:15"),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        )
      ),
    );
  }
}

class VideoItemMini extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.grey.shade800,
            borderRadius: BorderRadius.circular(8)
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Image.network(
              "https://i.pinimg.com/originals/78/e3/e3/78e3e3ec1e0bba2004a8fb1c87db47df.jpg",
              fit: BoxFit.cover,
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.3),
                  borderRadius: BorderRadius.circular(8.0)
              ),
              height: double.infinity,
              width: double.infinity,
              child: Icon(
                Icons.play_circle_outline,
                color: Colors.white,
                size: 36,
              ),
            ),
          ],
        )
    );
  }
}
