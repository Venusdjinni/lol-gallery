import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lolgallery/widgets/video_mini_list.dart';

class OfflineScreen extends StatefulWidget {
  final GlobalKey<NavigatorState> navKey;

  OfflineScreen(this.navKey);

  @override
  _OfflineScreenState createState() => _OfflineScreenState();
}

class _OfflineScreenState extends State<OfflineScreen> {
  @override
  Widget build(BuildContext context) {
    Widget body = Scaffold(
        appBar: AppBar(
          title: Text('Offline'),
        ),
        body: VideoMiniList()
    );

    return Navigator(
      key: widget.navKey,
      onGenerateRoute: (rs) =>
        MaterialPageRoute(
          builder: (context) => body
        ),
    );
  }
}
