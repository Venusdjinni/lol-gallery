import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'discover.dart';
import 'offline.dart';
import 'account.dart';

class HomeScreen extends StatefulWidget {
  static List<GlobalKey<NavigatorState>> _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>()
  ];

  List<Widget> screens = [
    DiscoverScreen(HomeScreen._navigatorKeys[0]),
    OfflineScreen(HomeScreen._navigatorKeys[1],),
    AccountScreen(HomeScreen._navigatorKeys[2],)
  ];

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;

  void _onTap(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          body: widget.screens[currentIndex],
          bottomNavigationBar: BottomNavigationBar(
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.album),
                  title: Text("Discover")
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.album),
                  title: Text("Offline")
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.album),
                  title: Text("Account")
              ),
            ],
            onTap: _onTap,
            currentIndex: currentIndex,
            selectedItemColor: Colors.blue,
          ),
        ),
        onWillPop: () async { return ! await HomeScreen._navigatorKeys[currentIndex].currentState.maybePop(); }
    );
  }

  /*@override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.album),
            title: Text("Discover")
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.album),
              title: Text("Offline")
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.album),
              title: Text("Account")
          ),
        ],
      ),
      tabBuilder: (context, int) {
        return CupertinoTabView(
          builder: (context) {
            switch(int) {
              case 1: return OfflineScreen();
              case 3: return AccountScreen();
              default: return DiscoverScreen();
            }
          },
          defaultTitle: (() {
            switch(int) {
              case 1: return "Offline";
              case 2: return "Account";
              default: return "Discover";
            }
          })(),
        );
      },
    );
  */
}
