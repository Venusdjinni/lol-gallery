import 'package:flutter/material.dart';
import 'package:lolgallery/widgets/video_mini_list.dart';

class MyVideos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My videos (58)"),
      ),
      body: VideoMiniList(),
    );
  }
}
