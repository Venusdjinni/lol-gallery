import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/screens/home.dart';
import 'package:lolgallery/screens/splash.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const String ROUTE_SIGN = "sign";
  static const String ROUTE_HOME = "home";

  Map<String, WidgetBuilder> routes = {
    ROUTE_SIGN: (context) => Splash(),
    ROUTE_HOME: (context) => HomeScreen(),
  };

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          elevation: 0,
          color: Colors.white,
        ),
        primaryColor: Colors.white,
        accentColor: Colors.grey.shade300,
        primaryColorDark: Colors.grey.shade700,
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
        primaryTextTheme: TextTheme(
          title: TextStyle(
            color: Colors.black
          )
        ),
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: Colors.white
        ),
      ),
      initialRoute: ROUTE_HOME,
      routes: routes,
    );
  }
}