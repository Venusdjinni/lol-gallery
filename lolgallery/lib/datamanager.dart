import 'package:lolgallery/modele/persistence.dart' as Persistence;

class DataManager {
  static DataManager _dm;
  static const String _server = "https://some.thing";
  static const String REQUEST_GET = "get";
  static const String REQUEST_POST = "post";

  static const String ROUTE_GET_USER_INFOS = "/users/of/";
  static const String ROUTE_POST_EDIT_USER = "/users/edit";
  static const String ROUTE_POST_FOLLOW_USER = "/users/follow";
  static const String ROUTE_GET_SEARCH_USER = "/users/search/";
  static const String ROUTE_POST_NEW_USER = "/users/new";

  static const String ROUTE_GET_LATEST_VIDEOS = "/videos/latest/";
  static const String ROUTE_GET_SEARCH_VIDEO = "/videos/search/";
  static const String ROUTE_GET_VIDEOS_OF = "/videos/of/";
  static const String ROUTE_POST_LIKE_VIDEO = "/videos/like";
  static const String ROUTE_POST_NEW_VIDEO = "/videos/new";
  static const String ROUTE_GET_VIEW_VIDEO = "/videos/";

  static const String ROUTE_GET_COMMENTS_OF = "/comments/of/";
  static const String ROUTE_POST_NEW_COMMENT = "/comments/new";
  static const String ROUTE_POST_LIKE_COMMENT = "/comments/like";

  static const String ROUTE_GET_TAG_VIDEOS = "/tags/videos/";

  static DataManager get() {
    if(_dm == null)
      _dm = new DataManager._();
    return _dm;
  }

  static Future<bool> init() {

    return DataManager.get()._init();
  }

  static Persistence.Database get db => _dm._database;

  DataManager._();

  //SharedPreferences _prefs;
  Persistence.Database _database;
  bool isInit = false;

  Future<bool> _init() async {
    if (isInit) return true;

    // Shared prefs
    //_prefs = await SharedPreferences.getInstance();
    // todo: save after login/signup
    _database = new Persistence.Database();

    return isInit = true;
  }

}