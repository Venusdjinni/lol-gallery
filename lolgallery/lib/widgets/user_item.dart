import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/screens/user_infos.dart';
import 'package:lolgallery/widgets/user_follow.dart';

class UserItem extends StatelessWidget {
  void onTap(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => UserInfos()
    ));
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => onTap(context),
      leading: CircleAvatar(
        backgroundColor: Colors.black,
        radius: 24,
      ),
      title: Text("Emily Thorne"),
      subtitle: Text("@emily.revenge7\n26k followers . 78 followings", overflow: TextOverflow.ellipsis,),
      isThreeLine: false,
      trailing: FollowUserButton(follow: true,),

    );
  }
}
