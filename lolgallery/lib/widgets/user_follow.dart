import 'package:flutter/material.dart';

class FollowUserButton extends StatefulWidget {
  bool follow;

  FollowUserButton({@required this.follow});

  @override
  _FollowUserButtonState createState() => _FollowUserButtonState();
}

class _FollowUserButtonState extends State<FollowUserButton> {
  void onPressed() {
    setState(() {
      widget.follow = !widget.follow;
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.follow ?
    FlatButton(
      onPressed: onPressed,
      color: Theme.of(context).primaryColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
        side: BorderSide(
          width: 1.0,
          color: Colors.black
        )
      ),
      child: Text("Abonné"),
    )
        :
    FlatButton(
      onPressed: onPressed,
      color: Theme.of(context).accentColor,
      child: Text("S'abonner"),
    );
  }
}
