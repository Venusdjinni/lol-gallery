import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/main.dart';

class SignupBirthdate extends StatefulWidget {
  @override
  _SignupBirthdateState createState() => _SignupBirthdateState();
}

class _SignupBirthdateState extends State<SignupBirthdate> {
  void onBirthdateSet(DateTime date) {

  }

  void setBirthdate() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(DateTime.now().year, DateTime.december, 31),
    ).then(onBirthdateSet);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: GestureDetector(
            onTap: setBirthdate,
            child: AbsorbPointer(
              child: TextFormField(
                decoration: InputDecoration(
                  hintText: "enter birthdate",
                  suffixIcon: Icon(Icons.date_range)
                ),
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FlatButton(
        onPressed: () => Navigator.of(context)
            .push(
            MaterialPageRoute(
                builder: (_) => SignupUsername()
            )
        ),
        child: Text("Next"),
      ),
    );
  }
}

class SignupUsername extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: CircleAvatar(
                  radius: 32,
                  backgroundColor: Colors.black,
                ),
              ),
              Expanded(
                child: Form(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(
                            hintText: "username"
                          ),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            prefixText: "@",
                            hintText: "nickname"
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
      floatingActionButton: FlatButton(
        onPressed: () => Navigator.pushNamedAndRemoveUntil(
            context,
            MyApp.ROUTE_HOME,
            (r) => false
        ),
        child: Text("Finish"),
      ),
    );
  }
}

