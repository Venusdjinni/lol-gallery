import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/screens/login.dart';
import 'package:lolgallery/screens/signup.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment(0, -0.2),
        children: <Widget>[
          FlutterLogo(
            size: 108,
          ),
          Align(
            alignment: Alignment(0, 0.4),
            child: Text(
              "Signup or login with",
              style: Theme.of(context).textTheme.subtitle,
            ),
          ),
          Align(
            alignment: Alignment(0, 0.7),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                GestureDetector(
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => Login())),
                  child: CircleAvatar(
                    radius: 24,
                    backgroundColor: Colors.white,
                    child: Image.asset("res/img/logo_fb.png")
                  ),
                ),
                SizedBox(width: 32,),
                CircleAvatar(
                  radius: 24,
                  backgroundColor: Colors.white,
                  child: Image.asset("res/img/logo_fb.png")
                ),
                SizedBox(width: 32,),
                GestureDetector(
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => SignupBirthdate())),
                  child: CircleAvatar(
                    radius: 24,
                    backgroundColor: Theme.of(context).accentColor,
                    child: Icon(Icons.call, color: Colors.black),
                  ),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  "Terms and services",
                  style: Theme.of(context).textTheme.overline.copyWith(
                    fontSize: 12
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
