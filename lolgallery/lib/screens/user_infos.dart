import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/widgets/user_follow.dart';
import 'package:lolgallery/widgets/video_mini_list.dart';

class UserInfos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Emily Thorne"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 72,
                width: 72,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                    width: 4
                  )
                ),
                margin: EdgeInsets.symmetric(horizontal: 16.0),
                child: Image.network("https://i.pinimg.com/originals/78/e3/e3/78e3e3ec1e0bba2004a8fb1c87db47df.jpg", fit: BoxFit.cover,),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "@emily.revenge7",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey.shade700
                      ),
                    ),
                    SizedBox(height: 16,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              "49",
                              style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                            SizedBox(height: 4,),
                            Text(
                              "Videos",
                              style: TextStyle(
                                fontSize: 12
                              ),
                            )
                          ],
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              "26.1k",
                              style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                            SizedBox(height: 4,),
                            Text(
                              "Followers",
                              style: TextStyle(
                                  fontSize: 12
                              ),
                            )
                          ],
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              "78",
                              style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                            SizedBox(height: 4,),
                            Text(
                              "Following",
                              style: TextStyle(
                                  fontSize: 12
                              ),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 16,),
          Center(child: FollowUserButton(follow: true,)),
          SizedBox(height: 16,),
          Container(
            height: 72,
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              "Ce guide s’adresse aux personnes désireuses d’effectuer une prise en main rapide du framework de développement mobile Flutter. En ce sens, il se contente de présenter les aspects fondamentaux",
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 8, left: 16, right: 16),
            child: Divider(),
          ),
          VideoMiniList()
        ],
      ),
    );
  }
}
