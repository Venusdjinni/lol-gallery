import 'package:moor_flutter/moor_flutter.dart';
import 'dart:async';

part 'persistence.g.dart';

class Videos extends Table {
  TextColumn get uuid => text()();
  DateTimeColumn get creationDate => dateTime()();
  DateTimeColumn get lastUpdate => dateTime()();

  @override
  Set<Column> get primaryKey => Set.of([uuid]);

  TextColumn get path => text()();
  // path for video thumbnail
  TextColumn get thumbnail => text()();
  TextColumn get description => text()();
  IntColumn get likes => integer()();
  IntColumn get commentaires => integer()();
}

class Users extends Table {
  TextColumn get uuid => text()();
  DateTimeColumn get creationDate => dateTime()();
  DateTimeColumn get lastUpdate => dateTime()();

  @override
  Set<Column> get primaryKey => Set.of([uuid]);

  TextColumn get username => text()();
  TextColumn get pseudonyme => text()();
  TextColumn get bio => text()();
  DateTimeColumn get birthdate => dateTime()();
  IntColumn get followers => integer()();
  IntColumn get following => integer()();
  IntColumn get videos => integer()();
}

class Tags extends Table {
  TextColumn get uuid => text()();
  DateTimeColumn get creationDate => dateTime()();
  DateTimeColumn get lastUpdate => dateTime()();

  @override
  Set<Column> get primaryKey => Set.of([uuid]);

  TextColumn get tag => text()();
  IntColumn get videos => integer()();
}

class Commentaires extends Table {
  TextColumn get uuid => text()();
  DateTimeColumn get creationDate => dateTime()();
  DateTimeColumn get lastUpdate => dateTime()();

  @override
  Set<Column> get primaryKey => Set.of([uuid]);

  TextColumn get message => text()();
  IntColumn get likes => integer()();
}

@UseMoor(tables: [
  Videos,
  Users,
  Tags,
  Commentaires
])
class Database extends _$Database {
  Database() : super(FlutterQueryExecutor.inDatabaseFolder(path: 'db.sqlite'));

  @override
  // TODO: implement schemaVersion
  int get schemaVersion => 1;
}