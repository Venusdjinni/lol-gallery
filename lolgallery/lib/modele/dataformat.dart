import 'package:lolgallery/modele/persistence.dart' as Persistence;

// todo: create toJson and fromJson methods
class Video {
  Persistence.Video video;
  User auteur;
  List<Persistence.Tag> tags;
}

class User {
  Persistence.User user;
}

class Commentaire {
  Persistence.Commentaire commentaire;
  User auteur;
}