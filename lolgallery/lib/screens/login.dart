import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/main.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: MediaQuery.of(context).size.height * 0.24,),
          CircleAvatar(
            radius: 54,
            backgroundColor: Colors.black,
          ),
          SizedBox(height: 16,),
          Text(
            "@alinooo22",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Theme.of(context).primaryColorDark,
              fontSize: 18
            ),
          ),
          SizedBox(height: 48,),
          Text(
            "Welcome back, Alain Parfait",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline,
          ),
          SizedBox(height: 48,),
        ],
      ),
      floatingActionButton: FlatButton(
        onPressed: () => Navigator.pushNamedAndRemoveUntil(
          context,
          MyApp.ROUTE_HOME,
          (r) => false
        ),
        child: Text("Thanks"),
      ),
    );
  }
}
