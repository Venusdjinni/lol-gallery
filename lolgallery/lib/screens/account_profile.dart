import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AccountProfile extends StatefulWidget {
  @override
  _AccountProfileState createState() => _AccountProfileState();
}

class _AccountProfileState extends State<AccountProfile> {
  @override
  Widget build(BuildContext context) {
    Widget divider = Container(
      padding: EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 4
      ),
      child: Divider(
        color: Colors.grey,
        height: 1.0,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Edit your profile"),
        actions: <Widget>[
          FlatButton(
            onPressed: () {},
            child: Text("Edit"),
            highlightColor: Theme.of(context).primaryColor,
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: Hero(
              tag: "pp",
              child: CircleAvatar(
                backgroundColor: Colors.black,
                radius: 54.0,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Text(
              "@alinooo22",
              style: Theme.of(context).textTheme.body1.copyWith(
                  color: Colors.grey.shade700,
                  fontStyle: FontStyle.italic
              )
            ),
          ),
          divider,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text("Alain Parfait")
                ),
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {}
                ),
              ],
            ),
          ),
          divider,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Text("La bio de quelqu'un\nAALLAOAO")
                ),
                IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () {}
                ),
              ],
            ),
          ),
          divider,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Text("20/11/1997")
                ),
                IconButton(
                    icon: Icon(Icons.calendar_today),
                    onPressed: () {}
                ),
              ],
            ),
          ),
          divider,
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 24),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Authentification\n",
                style: Theme.of(context).textTheme.subhead,
                children: [
                  TextSpan(
                    text: "Define multiple authentication methods to retrieve your account easier",
                    style: Theme.of(context).textTheme.body1.copyWith(
                      color: Colors.grey.shade700,
                      fontStyle: FontStyle.italic
                    )
                  )
                ]
              ),
            ),
          ),
          Align(
            alignment: Alignment(0, 0.7),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                GestureDetector(
                  onTap: () {},
                  child: Stack(
                    children: <Widget>[
                      CircleAvatar(
                        radius: 24,
                        backgroundColor: Colors.white,
                        child: Image.asset("res/img/logo_fb.png"),
                      ),
                      CircleAvatar(
                        backgroundColor: Colors.green.withOpacity(0.7),
                        radius: 24,
                        child: Icon(Icons.check, color: Colors.white,),
                      )
                    ],
                  ),
                ),
                SizedBox(width: 32,),
                CircleAvatar(
                  radius: 24,
                  backgroundColor: Colors.white,
                  child: Image.asset("res/img/logo_google.png")
                ),
                SizedBox(width: 32,),
                GestureDetector(
                  onTap: () {},
                  child: CircleAvatar(
                    radius: 24,
                    backgroundColor: Theme.of(context).accentColor,
                    child: Icon(Icons.call, color: Colors.black),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
