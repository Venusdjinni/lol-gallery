import 'package:flutter/material.dart';
import 'package:lolgallery/screens/tag_infos.dart';

class TagItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTagTap,
      child: ActionChip(
        label: Text("#montagprefere"),
        padding: EdgeInsets.zero,
        onPressed: () => onTagTap(context),
      ),
    );
  }

  void onTagTap(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(
          builder: (_) => TagInfos()
    ));
  }
}
