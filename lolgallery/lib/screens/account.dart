import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/widgets/video_mini_list.dart';
import 'account_followers.dart';
import 'account_profile.dart';
import 'package:image_picker/image_picker.dart';

import 'edit_video.dart';
import 'my_videos.dart';

class AccountScreen extends StatefulWidget {
  final GlobalKey<NavigatorState> navKey;

  AccountScreen(this.navKey);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    Widget body = Scaffold(
      appBar: AppBar(
        title: Text("Account"),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.of(context)
                .push(MaterialPageRoute(
                builder: (_) => AccountProfile()
            )),
            child: Card(
              margin: EdgeInsets.symmetric(horizontal: 16.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              color: Colors.blue.shade50,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Profil",
                      style: Theme.of(context).textTheme.subhead,
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 16.0,
                              left: 16.0,
                              right: 24.0,
                              bottom: 16.0
                          ),
                          child: Hero(
                            tag: "pp",
                            child: CircleAvatar(
                              backgroundColor: Colors.black,
                              radius: 48.0,
                            ),
                          ),
                        ),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Hero(
                                tag: "pseudo",
                                child: Text(
                                  "Alain Parfait",
                                  style: Theme.of(context).textTheme.headline,
                                ),
                              ),
                              Hero(
                                tag: "username",
                                child: Text(
                                  "@alinooo22",
                                  style: Theme.of(context).textTheme.subhead.copyWith(
                                    color: Colors.grey.shade700,
                                  ),
                                ),
                              ),
                              SizedBox(height: 8.0,),
                              Hero(
                                tag: "bio",
                                child: Text("La bio de quelqu'un"),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 16.0,),
          Card(
            margin: EdgeInsets.symmetric(horizontal: 16.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            color: Colors.orange.shade50,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Abonnements",
                    style: Theme.of(context).textTheme.subhead,
                  ),
                  SizedBox(height: 8.0,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Flexible(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) => AccountFollowers()
                              ));
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.arrow_downward,
                                    size: 32.0,
                                  ),
                                ),
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "200",
                                      style: Theme.of(context).textTheme.headline,
                                    ),
                                    Text("abonnés")
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Flexible(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) => AccountFollowers()
                              ));
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.arrow_upward,
                                    size: 32.0,
                                  ),
                                ),
                                Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "46",
                                      style: Theme.of(context).textTheme.headline,
                                    ),
                                    Text("abonnements")
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 16.0,),
          GestureDetector(
            onTap: () => Navigator.of(context)
                .push(MaterialPageRoute(
                builder: (_) => MyVideos()
            )),
            child: Card(
              margin: EdgeInsets.symmetric(horizontal: 16.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              color: Colors.green.shade50,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8.0,
                  vertical: 16.0
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "Vidéos",
                        style: Theme.of(context).textTheme.subhead,
                      ),
                    ),
                    Text(
                      "8",
                      style: Theme.of(context).textTheme.subhead,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: Icon(
                        Icons.navigate_next,
                        size: 32,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: CustomFloatingButton(_createContent),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );

    return Navigator(
      key: widget.navKey,
      onGenerateRoute: (rs) =>
          MaterialPageRoute(
              builder: (context) => body
          ),
    );
  }

  Future _createContent() async {
    File video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    if (video != null)
      Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => EditVideo(video: video,)
      ));
  }
}

class CustomFloatingButton extends StatelessWidget {
  final VoidCallback _onCreate;

  const CustomFloatingButton(this._onCreate, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onCreate,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20)
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.grey.shade900,
            borderRadius: BorderRadius.circular(20)
          ),
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 8.0
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                Icons.videocam,
                color: Colors.white,
              ),
              SizedBox(width: 8,),
              Text(
                "Create",
                style: TextStyle(color: Colors.white),
              )
            ],
          ),
        ),
      ),
    );
  }
}


