import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/screens/user_infos.dart';
import 'package:lolgallery/widgets/comment_item.dart';
import 'package:lolgallery/widgets/tag_item.dart';

class VideoComments extends StatefulWidget {
  @override
  _VideoCommentsState createState() => _VideoCommentsState();
}

class _VideoCommentsState extends State<VideoComments> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Comments"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
           children: <Widget>[
             Container(
               height: 72,
               width: 72,
               decoration: BoxDecoration(
                   border: Border.all(
                       color: Colors.black,
                       width: 4
                   )
               ),
               margin: EdgeInsets.symmetric(horizontal: 16.0),
               child: Image.network("https://i.pinimg.com/originals/78/e3/e3/78e3e3ec1e0bba2004a8fb1c87db47df.jpg", fit: BoxFit.cover,),
             ),
             Expanded(
               child: Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 mainAxisSize: MainAxisSize.min,
                 children: <Widget>[
                   Text("Lorem ipsum dolor sit amet consecutur nana ni nana na. Bon sang de bonsoir",),
                   SizedBox(height: 8.0,),
                   GestureDetector(
                     onTap: () =>
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => UserInfos()
                      )),
                     child: RichText(
                       overflow: TextOverflow.ellipsis,
                       text: TextSpan(
                         style: Theme.of(context).textTheme.subtitle
                             .copyWith(fontWeight: FontWeight.normal),
                         text: "By ",
                         children: [
                           TextSpan(
                             style: Theme.of(context).textTheme.subtitle,
                             text: "Emily Thorne"
                           )
                         ]
                       ),
                     ),
                   ),
                 ],
               ),
             )
           ],
          ),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Wrap(
              spacing: 4.0,
              children: <Widget>[
                Chip(label: Text("#tag1"), padding: EdgeInsets.zero,),
                TagItem(),
                Chip(label: Text("#baongla"), padding: EdgeInsets.zero,),
                Chip(label: Text("#jenaipaslargent"), padding: EdgeInsets.zero,),
                Chip(label: Text("#foryou"), padding: EdgeInsets.zero,),
                Chip(label: Text("#foryoupage"), padding: EdgeInsets.zero,),
                Chip(label: Text("#mdr"), padding: EdgeInsets.zero,),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 8.0),
            child: Divider(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
              "200 comments",
              style: Theme.of(context).textTheme.subhead,
            ),
          ),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 8.0
              ),
              itemBuilder: (context, index) =>
                  CommentItem(),
              separatorBuilder: (context, index) =>
                  SizedBox(height: 4.0,),
              itemCount: 20,
              //shrinkWrap: true,
              //physics: PageScrollPhysics(),
            ),
          )
        ],
      ),
    );
  }
}