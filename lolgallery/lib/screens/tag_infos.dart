import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/widgets/user_follow.dart';
import 'package:lolgallery/widgets/video_mini_list.dart';

class TagInfos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("#montagprefere"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 72,
                width: 72,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black,
                      width: 4
                  ),
                  color: Theme.of(context).accentColor,
                ),
                margin: EdgeInsets.symmetric(horizontal: 16.0),
                child: Text(
                  "M",
                  style: Theme.of(context).textTheme.display2.copyWith(
                    color: Colors.black
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "82 vidéos",
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.black
                      ),
                    ),
                    SizedBox(height: 8,),
                    Text(
                      "1200 likes",
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.grey.shade700
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          Container(
            padding: EdgeInsets.only(top: 8, left: 16, right: 16),
            child: Divider(),
          ),
          //SizedBox(height: 16,),
          Expanded(child: VideoMiniList())
        ],
      ),
    );
  }
}
