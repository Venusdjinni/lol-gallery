import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CommentItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
          maxWidth: 300
      ),
      child: Container(
        /*decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(8)
        ),*/
        //padding: EdgeInsets.all(16),
        child: Row(
          children: <Widget>[
            CircleAvatar(
              radius: 16,
              backgroundImage: NetworkImage("https://i.pinimg.com/originals/78/e3/e3/78e3e3ec1e0bba2004a8fb1c87db47df.jpg"),
            ),
            SizedBox(width: 4,),
            Expanded(
              child: Card(
                elevation: 1.0,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 8.0,
                      vertical: 4.0
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "Arnold Loic",
                        style: Theme.of(context).textTheme.body2,
                      ),
                      SizedBox(height: 4,),
                      Text(
                        "Lorem ipsum dolor sit amet consecutur nana ni nana na. Bon sang de bonsoir",
                        style: Theme.of(context).textTheme.body1.copyWith(
                          fontSize: 12
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(width: 4,),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.favorite_border),
                SizedBox(height: 2,),
                Text(
                  "30",
                  style: Theme.of(context).textTheme.body1.copyWith(
                      fontSize: 12
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
