import 'package:flutter/material.dart';
import 'video_item.dart';

class VideoMiniList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 8.0,
        crossAxisSpacing: 8.0,
        childAspectRatio: 3/4
      ),
      itemBuilder: (context, index) =>
          VideoItemMini(),
      itemCount: 10,
      physics: PageScrollPhysics(),
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
        vertical: 4.0
      ),
    );
  }
}
