import 'package:flutter/material.dart';
import 'package:lolgallery/widgets/user_item.dart';

class AccountFollowers extends StatefulWidget {
  @override
  _AccountFollowersState createState() => _AccountFollowersState();
}

class _AccountFollowersState extends State<AccountFollowers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text("Your followers"),
      ),
      body: ListView.separated(
        itemBuilder: (context, index) =>
          UserItem(),
        separatorBuilder: (context, index) =>
          Divider(
            height: 1.0,
          ),
        itemCount: 20,
      ),
    );
  }
}
