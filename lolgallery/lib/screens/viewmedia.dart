import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lolgallery/screens/video_comments.dart';

class ViewMediaScreen extends StatefulWidget {
  @override
  _ViewMediaScreenState createState() => _ViewMediaScreenState();
}

class _ViewMediaScreenState extends State<ViewMediaScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.black,
        ),
        Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            title: Text(""),
            iconTheme: IconThemeData(
              color: Theme.of(context).backgroundColor
            ),
          ),
          backgroundColor: Colors.transparent,
          body: Stack(
            children: <Widget>[
              Align(
                child: CircularProgressIndicator(),
                alignment: Alignment.center,
              ),
              Container(
                alignment: Alignment.bottomRight,
                padding: EdgeInsets.only(bottom: 50.0),
                child: Container(
                  height: 170.0,
                  margin: EdgeInsets.only(right: 12.0, bottom: 12.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      FloatingActionButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (_) => Dialog(
                              backgroundColor: Colors.transparent,
                              child: EmojiCommentPopup(),
                            )
                          );
                        },
                        mini: true,
                        heroTag: null,
                        child: Icon(Icons.insert_emoticon, color: Colors.green,),
                      ),
                      FloatingActionButton(
                        onPressed: () {
                          Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (_) => VideoComments()
                              )
                          );
                        },
                        heroTag: null,
                        mini: true,
                      ),
                      FloatingActionButton(
                        onPressed: () {},
                        heroTag: null,
                        mini: true,
                        child: Icon(Icons.file_download),
                      )
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Divider(
                      color: Colors.grey,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: 16.0,
                      ),
                      child: TextField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Mon commentaire...",
                            hintStyle: TextStyle(color: Colors.white.withOpacity(0.4))
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}

class EmojiCommentPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 6.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10)
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 24
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              "res/img/emoji_laugh.png",
              height: 64,
              width: 64,
            ),
            Image.asset(
              "res/img/emoji_baby.png",
              height: 64,
              width: 64,
            )
          ],
        ),
      ),
    );
  }
}


