// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'persistence.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps
class Video extends DataClass implements Insertable<Video> {
  final String uuid;
  final DateTime creationDate;
  final DateTime lastUpdate;
  final String path;
  final String thumbnail;
  final String description;
  final int likes;
  final int commentaires;
  Video(
      {@required this.uuid,
      @required this.creationDate,
      @required this.lastUpdate,
      @required this.path,
      @required this.thumbnail,
      @required this.description,
      @required this.likes,
      @required this.commentaires});
  factory Video.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    final intType = db.typeSystem.forDartType<int>();
    return Video(
      uuid: stringType.mapFromDatabaseResponse(data['${effectivePrefix}uuid']),
      creationDate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}creation_date']),
      lastUpdate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}last_update']),
      path: stringType.mapFromDatabaseResponse(data['${effectivePrefix}path']),
      thumbnail: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}thumbnail']),
      description: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}description']),
      likes: intType.mapFromDatabaseResponse(data['${effectivePrefix}likes']),
      commentaires: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}commentaires']),
    );
  }
  factory Video.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Video(
      uuid: serializer.fromJson<String>(json['uuid']),
      creationDate: serializer.fromJson<DateTime>(json['creationDate']),
      lastUpdate: serializer.fromJson<DateTime>(json['lastUpdate']),
      path: serializer.fromJson<String>(json['path']),
      thumbnail: serializer.fromJson<String>(json['thumbnail']),
      description: serializer.fromJson<String>(json['description']),
      likes: serializer.fromJson<int>(json['likes']),
      commentaires: serializer.fromJson<int>(json['commentaires']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'uuid': serializer.toJson<String>(uuid),
      'creationDate': serializer.toJson<DateTime>(creationDate),
      'lastUpdate': serializer.toJson<DateTime>(lastUpdate),
      'path': serializer.toJson<String>(path),
      'thumbnail': serializer.toJson<String>(thumbnail),
      'description': serializer.toJson<String>(description),
      'likes': serializer.toJson<int>(likes),
      'commentaires': serializer.toJson<int>(commentaires),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Video>>(bool nullToAbsent) {
    return VideosCompanion(
      uuid: uuid == null && nullToAbsent ? const Value.absent() : Value(uuid),
      creationDate: creationDate == null && nullToAbsent
          ? const Value.absent()
          : Value(creationDate),
      lastUpdate: lastUpdate == null && nullToAbsent
          ? const Value.absent()
          : Value(lastUpdate),
      path: path == null && nullToAbsent ? const Value.absent() : Value(path),
      thumbnail: thumbnail == null && nullToAbsent
          ? const Value.absent()
          : Value(thumbnail),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      likes:
          likes == null && nullToAbsent ? const Value.absent() : Value(likes),
      commentaires: commentaires == null && nullToAbsent
          ? const Value.absent()
          : Value(commentaires),
    ) as T;
  }

  Video copyWith(
          {String uuid,
          DateTime creationDate,
          DateTime lastUpdate,
          String path,
          String thumbnail,
          String description,
          int likes,
          int commentaires}) =>
      Video(
        uuid: uuid ?? this.uuid,
        creationDate: creationDate ?? this.creationDate,
        lastUpdate: lastUpdate ?? this.lastUpdate,
        path: path ?? this.path,
        thumbnail: thumbnail ?? this.thumbnail,
        description: description ?? this.description,
        likes: likes ?? this.likes,
        commentaires: commentaires ?? this.commentaires,
      );
  @override
  String toString() {
    return (StringBuffer('Video(')
          ..write('uuid: $uuid, ')
          ..write('creationDate: $creationDate, ')
          ..write('lastUpdate: $lastUpdate, ')
          ..write('path: $path, ')
          ..write('thumbnail: $thumbnail, ')
          ..write('description: $description, ')
          ..write('likes: $likes, ')
          ..write('commentaires: $commentaires')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      uuid.hashCode,
      $mrjc(
          creationDate.hashCode,
          $mrjc(
              lastUpdate.hashCode,
              $mrjc(
                  path.hashCode,
                  $mrjc(
                      thumbnail.hashCode,
                      $mrjc(description.hashCode,
                          $mrjc(likes.hashCode, commentaires.hashCode))))))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Video &&
          other.uuid == uuid &&
          other.creationDate == creationDate &&
          other.lastUpdate == lastUpdate &&
          other.path == path &&
          other.thumbnail == thumbnail &&
          other.description == description &&
          other.likes == likes &&
          other.commentaires == commentaires);
}

class VideosCompanion extends UpdateCompanion<Video> {
  final Value<String> uuid;
  final Value<DateTime> creationDate;
  final Value<DateTime> lastUpdate;
  final Value<String> path;
  final Value<String> thumbnail;
  final Value<String> description;
  final Value<int> likes;
  final Value<int> commentaires;
  const VideosCompanion({
    this.uuid = const Value.absent(),
    this.creationDate = const Value.absent(),
    this.lastUpdate = const Value.absent(),
    this.path = const Value.absent(),
    this.thumbnail = const Value.absent(),
    this.description = const Value.absent(),
    this.likes = const Value.absent(),
    this.commentaires = const Value.absent(),
  });
  VideosCompanion copyWith(
      {Value<String> uuid,
      Value<DateTime> creationDate,
      Value<DateTime> lastUpdate,
      Value<String> path,
      Value<String> thumbnail,
      Value<String> description,
      Value<int> likes,
      Value<int> commentaires}) {
    return VideosCompanion(
      uuid: uuid ?? this.uuid,
      creationDate: creationDate ?? this.creationDate,
      lastUpdate: lastUpdate ?? this.lastUpdate,
      path: path ?? this.path,
      thumbnail: thumbnail ?? this.thumbnail,
      description: description ?? this.description,
      likes: likes ?? this.likes,
      commentaires: commentaires ?? this.commentaires,
    );
  }
}

class $VideosTable extends Videos with TableInfo<$VideosTable, Video> {
  final GeneratedDatabase _db;
  final String _alias;
  $VideosTable(this._db, [this._alias]);
  final VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  GeneratedTextColumn _uuid;
  @override
  GeneratedTextColumn get uuid => _uuid ??= _constructUuid();
  GeneratedTextColumn _constructUuid() {
    return GeneratedTextColumn(
      'uuid',
      $tableName,
      false,
    );
  }

  final VerificationMeta _creationDateMeta =
      const VerificationMeta('creationDate');
  GeneratedDateTimeColumn _creationDate;
  @override
  GeneratedDateTimeColumn get creationDate =>
      _creationDate ??= _constructCreationDate();
  GeneratedDateTimeColumn _constructCreationDate() {
    return GeneratedDateTimeColumn(
      'creation_date',
      $tableName,
      false,
    );
  }

  final VerificationMeta _lastUpdateMeta = const VerificationMeta('lastUpdate');
  GeneratedDateTimeColumn _lastUpdate;
  @override
  GeneratedDateTimeColumn get lastUpdate =>
      _lastUpdate ??= _constructLastUpdate();
  GeneratedDateTimeColumn _constructLastUpdate() {
    return GeneratedDateTimeColumn(
      'last_update',
      $tableName,
      false,
    );
  }

  final VerificationMeta _pathMeta = const VerificationMeta('path');
  GeneratedTextColumn _path;
  @override
  GeneratedTextColumn get path => _path ??= _constructPath();
  GeneratedTextColumn _constructPath() {
    return GeneratedTextColumn(
      'path',
      $tableName,
      false,
    );
  }

  final VerificationMeta _thumbnailMeta = const VerificationMeta('thumbnail');
  GeneratedTextColumn _thumbnail;
  @override
  GeneratedTextColumn get thumbnail => _thumbnail ??= _constructThumbnail();
  GeneratedTextColumn _constructThumbnail() {
    return GeneratedTextColumn(
      'thumbnail',
      $tableName,
      false,
    );
  }

  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  GeneratedTextColumn _description;
  @override
  GeneratedTextColumn get description =>
      _description ??= _constructDescription();
  GeneratedTextColumn _constructDescription() {
    return GeneratedTextColumn(
      'description',
      $tableName,
      false,
    );
  }

  final VerificationMeta _likesMeta = const VerificationMeta('likes');
  GeneratedIntColumn _likes;
  @override
  GeneratedIntColumn get likes => _likes ??= _constructLikes();
  GeneratedIntColumn _constructLikes() {
    return GeneratedIntColumn(
      'likes',
      $tableName,
      false,
    );
  }

  final VerificationMeta _commentairesMeta =
      const VerificationMeta('commentaires');
  GeneratedIntColumn _commentaires;
  @override
  GeneratedIntColumn get commentaires =>
      _commentaires ??= _constructCommentaires();
  GeneratedIntColumn _constructCommentaires() {
    return GeneratedIntColumn(
      'commentaires',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        creationDate,
        lastUpdate,
        path,
        thumbnail,
        description,
        likes,
        commentaires
      ];
  @override
  $VideosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'videos';
  @override
  final String actualTableName = 'videos';
  @override
  VerificationContext validateIntegrity(VideosCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.uuid.present) {
      context.handle(
          _uuidMeta, uuid.isAcceptableValue(d.uuid.value, _uuidMeta));
    } else if (uuid.isRequired && isInserting) {
      context.missing(_uuidMeta);
    }
    if (d.creationDate.present) {
      context.handle(
          _creationDateMeta,
          creationDate.isAcceptableValue(
              d.creationDate.value, _creationDateMeta));
    } else if (creationDate.isRequired && isInserting) {
      context.missing(_creationDateMeta);
    }
    if (d.lastUpdate.present) {
      context.handle(_lastUpdateMeta,
          lastUpdate.isAcceptableValue(d.lastUpdate.value, _lastUpdateMeta));
    } else if (lastUpdate.isRequired && isInserting) {
      context.missing(_lastUpdateMeta);
    }
    if (d.path.present) {
      context.handle(
          _pathMeta, path.isAcceptableValue(d.path.value, _pathMeta));
    } else if (path.isRequired && isInserting) {
      context.missing(_pathMeta);
    }
    if (d.thumbnail.present) {
      context.handle(_thumbnailMeta,
          thumbnail.isAcceptableValue(d.thumbnail.value, _thumbnailMeta));
    } else if (thumbnail.isRequired && isInserting) {
      context.missing(_thumbnailMeta);
    }
    if (d.description.present) {
      context.handle(_descriptionMeta,
          description.isAcceptableValue(d.description.value, _descriptionMeta));
    } else if (description.isRequired && isInserting) {
      context.missing(_descriptionMeta);
    }
    if (d.likes.present) {
      context.handle(
          _likesMeta, likes.isAcceptableValue(d.likes.value, _likesMeta));
    } else if (likes.isRequired && isInserting) {
      context.missing(_likesMeta);
    }
    if (d.commentaires.present) {
      context.handle(
          _commentairesMeta,
          commentaires.isAcceptableValue(
              d.commentaires.value, _commentairesMeta));
    } else if (commentaires.isRequired && isInserting) {
      context.missing(_commentairesMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Video map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Video.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(VideosCompanion d) {
    final map = <String, Variable>{};
    if (d.uuid.present) {
      map['uuid'] = Variable<String, StringType>(d.uuid.value);
    }
    if (d.creationDate.present) {
      map['creation_date'] =
          Variable<DateTime, DateTimeType>(d.creationDate.value);
    }
    if (d.lastUpdate.present) {
      map['last_update'] = Variable<DateTime, DateTimeType>(d.lastUpdate.value);
    }
    if (d.path.present) {
      map['path'] = Variable<String, StringType>(d.path.value);
    }
    if (d.thumbnail.present) {
      map['thumbnail'] = Variable<String, StringType>(d.thumbnail.value);
    }
    if (d.description.present) {
      map['description'] = Variable<String, StringType>(d.description.value);
    }
    if (d.likes.present) {
      map['likes'] = Variable<int, IntType>(d.likes.value);
    }
    if (d.commentaires.present) {
      map['commentaires'] = Variable<int, IntType>(d.commentaires.value);
    }
    return map;
  }

  @override
  $VideosTable createAlias(String alias) {
    return $VideosTable(_db, alias);
  }
}

class User extends DataClass implements Insertable<User> {
  final String uuid;
  final DateTime creationDate;
  final DateTime lastUpdate;
  final String username;
  final String pseudonyme;
  final String bio;
  final DateTime birthdate;
  final int followers;
  final int following;
  final int videos;
  User(
      {@required this.uuid,
      @required this.creationDate,
      @required this.lastUpdate,
      @required this.username,
      @required this.pseudonyme,
      @required this.bio,
      @required this.birthdate,
      @required this.followers,
      @required this.following,
      @required this.videos});
  factory User.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    final intType = db.typeSystem.forDartType<int>();
    return User(
      uuid: stringType.mapFromDatabaseResponse(data['${effectivePrefix}uuid']),
      creationDate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}creation_date']),
      lastUpdate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}last_update']),
      username: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}username']),
      pseudonyme: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}pseudonyme']),
      bio: stringType.mapFromDatabaseResponse(data['${effectivePrefix}bio']),
      birthdate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}birthdate']),
      followers:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}followers']),
      following:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}following']),
      videos: intType.mapFromDatabaseResponse(data['${effectivePrefix}videos']),
    );
  }
  factory User.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return User(
      uuid: serializer.fromJson<String>(json['uuid']),
      creationDate: serializer.fromJson<DateTime>(json['creationDate']),
      lastUpdate: serializer.fromJson<DateTime>(json['lastUpdate']),
      username: serializer.fromJson<String>(json['username']),
      pseudonyme: serializer.fromJson<String>(json['pseudonyme']),
      bio: serializer.fromJson<String>(json['bio']),
      birthdate: serializer.fromJson<DateTime>(json['birthdate']),
      followers: serializer.fromJson<int>(json['followers']),
      following: serializer.fromJson<int>(json['following']),
      videos: serializer.fromJson<int>(json['videos']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'uuid': serializer.toJson<String>(uuid),
      'creationDate': serializer.toJson<DateTime>(creationDate),
      'lastUpdate': serializer.toJson<DateTime>(lastUpdate),
      'username': serializer.toJson<String>(username),
      'pseudonyme': serializer.toJson<String>(pseudonyme),
      'bio': serializer.toJson<String>(bio),
      'birthdate': serializer.toJson<DateTime>(birthdate),
      'followers': serializer.toJson<int>(followers),
      'following': serializer.toJson<int>(following),
      'videos': serializer.toJson<int>(videos),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<User>>(bool nullToAbsent) {
    return UsersCompanion(
      uuid: uuid == null && nullToAbsent ? const Value.absent() : Value(uuid),
      creationDate: creationDate == null && nullToAbsent
          ? const Value.absent()
          : Value(creationDate),
      lastUpdate: lastUpdate == null && nullToAbsent
          ? const Value.absent()
          : Value(lastUpdate),
      username: username == null && nullToAbsent
          ? const Value.absent()
          : Value(username),
      pseudonyme: pseudonyme == null && nullToAbsent
          ? const Value.absent()
          : Value(pseudonyme),
      bio: bio == null && nullToAbsent ? const Value.absent() : Value(bio),
      birthdate: birthdate == null && nullToAbsent
          ? const Value.absent()
          : Value(birthdate),
      followers: followers == null && nullToAbsent
          ? const Value.absent()
          : Value(followers),
      following: following == null && nullToAbsent
          ? const Value.absent()
          : Value(following),
      videos:
          videos == null && nullToAbsent ? const Value.absent() : Value(videos),
    ) as T;
  }

  User copyWith(
          {String uuid,
          DateTime creationDate,
          DateTime lastUpdate,
          String username,
          String pseudonyme,
          String bio,
          DateTime birthdate,
          int followers,
          int following,
          int videos}) =>
      User(
        uuid: uuid ?? this.uuid,
        creationDate: creationDate ?? this.creationDate,
        lastUpdate: lastUpdate ?? this.lastUpdate,
        username: username ?? this.username,
        pseudonyme: pseudonyme ?? this.pseudonyme,
        bio: bio ?? this.bio,
        birthdate: birthdate ?? this.birthdate,
        followers: followers ?? this.followers,
        following: following ?? this.following,
        videos: videos ?? this.videos,
      );
  @override
  String toString() {
    return (StringBuffer('User(')
          ..write('uuid: $uuid, ')
          ..write('creationDate: $creationDate, ')
          ..write('lastUpdate: $lastUpdate, ')
          ..write('username: $username, ')
          ..write('pseudonyme: $pseudonyme, ')
          ..write('bio: $bio, ')
          ..write('birthdate: $birthdate, ')
          ..write('followers: $followers, ')
          ..write('following: $following, ')
          ..write('videos: $videos')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      uuid.hashCode,
      $mrjc(
          creationDate.hashCode,
          $mrjc(
              lastUpdate.hashCode,
              $mrjc(
                  username.hashCode,
                  $mrjc(
                      pseudonyme.hashCode,
                      $mrjc(
                          bio.hashCode,
                          $mrjc(
                              birthdate.hashCode,
                              $mrjc(
                                  followers.hashCode,
                                  $mrjc(following.hashCode,
                                      videos.hashCode))))))))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is User &&
          other.uuid == uuid &&
          other.creationDate == creationDate &&
          other.lastUpdate == lastUpdate &&
          other.username == username &&
          other.pseudonyme == pseudonyme &&
          other.bio == bio &&
          other.birthdate == birthdate &&
          other.followers == followers &&
          other.following == following &&
          other.videos == videos);
}

class UsersCompanion extends UpdateCompanion<User> {
  final Value<String> uuid;
  final Value<DateTime> creationDate;
  final Value<DateTime> lastUpdate;
  final Value<String> username;
  final Value<String> pseudonyme;
  final Value<String> bio;
  final Value<DateTime> birthdate;
  final Value<int> followers;
  final Value<int> following;
  final Value<int> videos;
  const UsersCompanion({
    this.uuid = const Value.absent(),
    this.creationDate = const Value.absent(),
    this.lastUpdate = const Value.absent(),
    this.username = const Value.absent(),
    this.pseudonyme = const Value.absent(),
    this.bio = const Value.absent(),
    this.birthdate = const Value.absent(),
    this.followers = const Value.absent(),
    this.following = const Value.absent(),
    this.videos = const Value.absent(),
  });
  UsersCompanion copyWith(
      {Value<String> uuid,
      Value<DateTime> creationDate,
      Value<DateTime> lastUpdate,
      Value<String> username,
      Value<String> pseudonyme,
      Value<String> bio,
      Value<DateTime> birthdate,
      Value<int> followers,
      Value<int> following,
      Value<int> videos}) {
    return UsersCompanion(
      uuid: uuid ?? this.uuid,
      creationDate: creationDate ?? this.creationDate,
      lastUpdate: lastUpdate ?? this.lastUpdate,
      username: username ?? this.username,
      pseudonyme: pseudonyme ?? this.pseudonyme,
      bio: bio ?? this.bio,
      birthdate: birthdate ?? this.birthdate,
      followers: followers ?? this.followers,
      following: following ?? this.following,
      videos: videos ?? this.videos,
    );
  }
}

class $UsersTable extends Users with TableInfo<$UsersTable, User> {
  final GeneratedDatabase _db;
  final String _alias;
  $UsersTable(this._db, [this._alias]);
  final VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  GeneratedTextColumn _uuid;
  @override
  GeneratedTextColumn get uuid => _uuid ??= _constructUuid();
  GeneratedTextColumn _constructUuid() {
    return GeneratedTextColumn(
      'uuid',
      $tableName,
      false,
    );
  }

  final VerificationMeta _creationDateMeta =
      const VerificationMeta('creationDate');
  GeneratedDateTimeColumn _creationDate;
  @override
  GeneratedDateTimeColumn get creationDate =>
      _creationDate ??= _constructCreationDate();
  GeneratedDateTimeColumn _constructCreationDate() {
    return GeneratedDateTimeColumn(
      'creation_date',
      $tableName,
      false,
    );
  }

  final VerificationMeta _lastUpdateMeta = const VerificationMeta('lastUpdate');
  GeneratedDateTimeColumn _lastUpdate;
  @override
  GeneratedDateTimeColumn get lastUpdate =>
      _lastUpdate ??= _constructLastUpdate();
  GeneratedDateTimeColumn _constructLastUpdate() {
    return GeneratedDateTimeColumn(
      'last_update',
      $tableName,
      false,
    );
  }

  final VerificationMeta _usernameMeta = const VerificationMeta('username');
  GeneratedTextColumn _username;
  @override
  GeneratedTextColumn get username => _username ??= _constructUsername();
  GeneratedTextColumn _constructUsername() {
    return GeneratedTextColumn(
      'username',
      $tableName,
      false,
    );
  }

  final VerificationMeta _pseudonymeMeta = const VerificationMeta('pseudonyme');
  GeneratedTextColumn _pseudonyme;
  @override
  GeneratedTextColumn get pseudonyme => _pseudonyme ??= _constructPseudonyme();
  GeneratedTextColumn _constructPseudonyme() {
    return GeneratedTextColumn(
      'pseudonyme',
      $tableName,
      false,
    );
  }

  final VerificationMeta _bioMeta = const VerificationMeta('bio');
  GeneratedTextColumn _bio;
  @override
  GeneratedTextColumn get bio => _bio ??= _constructBio();
  GeneratedTextColumn _constructBio() {
    return GeneratedTextColumn(
      'bio',
      $tableName,
      false,
    );
  }

  final VerificationMeta _birthdateMeta = const VerificationMeta('birthdate');
  GeneratedDateTimeColumn _birthdate;
  @override
  GeneratedDateTimeColumn get birthdate => _birthdate ??= _constructBirthdate();
  GeneratedDateTimeColumn _constructBirthdate() {
    return GeneratedDateTimeColumn(
      'birthdate',
      $tableName,
      false,
    );
  }

  final VerificationMeta _followersMeta = const VerificationMeta('followers');
  GeneratedIntColumn _followers;
  @override
  GeneratedIntColumn get followers => _followers ??= _constructFollowers();
  GeneratedIntColumn _constructFollowers() {
    return GeneratedIntColumn(
      'followers',
      $tableName,
      false,
    );
  }

  final VerificationMeta _followingMeta = const VerificationMeta('following');
  GeneratedIntColumn _following;
  @override
  GeneratedIntColumn get following => _following ??= _constructFollowing();
  GeneratedIntColumn _constructFollowing() {
    return GeneratedIntColumn(
      'following',
      $tableName,
      false,
    );
  }

  final VerificationMeta _videosMeta = const VerificationMeta('videos');
  GeneratedIntColumn _videos;
  @override
  GeneratedIntColumn get videos => _videos ??= _constructVideos();
  GeneratedIntColumn _constructVideos() {
    return GeneratedIntColumn(
      'videos',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        creationDate,
        lastUpdate,
        username,
        pseudonyme,
        bio,
        birthdate,
        followers,
        following,
        videos
      ];
  @override
  $UsersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'users';
  @override
  final String actualTableName = 'users';
  @override
  VerificationContext validateIntegrity(UsersCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.uuid.present) {
      context.handle(
          _uuidMeta, uuid.isAcceptableValue(d.uuid.value, _uuidMeta));
    } else if (uuid.isRequired && isInserting) {
      context.missing(_uuidMeta);
    }
    if (d.creationDate.present) {
      context.handle(
          _creationDateMeta,
          creationDate.isAcceptableValue(
              d.creationDate.value, _creationDateMeta));
    } else if (creationDate.isRequired && isInserting) {
      context.missing(_creationDateMeta);
    }
    if (d.lastUpdate.present) {
      context.handle(_lastUpdateMeta,
          lastUpdate.isAcceptableValue(d.lastUpdate.value, _lastUpdateMeta));
    } else if (lastUpdate.isRequired && isInserting) {
      context.missing(_lastUpdateMeta);
    }
    if (d.username.present) {
      context.handle(_usernameMeta,
          username.isAcceptableValue(d.username.value, _usernameMeta));
    } else if (username.isRequired && isInserting) {
      context.missing(_usernameMeta);
    }
    if (d.pseudonyme.present) {
      context.handle(_pseudonymeMeta,
          pseudonyme.isAcceptableValue(d.pseudonyme.value, _pseudonymeMeta));
    } else if (pseudonyme.isRequired && isInserting) {
      context.missing(_pseudonymeMeta);
    }
    if (d.bio.present) {
      context.handle(_bioMeta, bio.isAcceptableValue(d.bio.value, _bioMeta));
    } else if (bio.isRequired && isInserting) {
      context.missing(_bioMeta);
    }
    if (d.birthdate.present) {
      context.handle(_birthdateMeta,
          birthdate.isAcceptableValue(d.birthdate.value, _birthdateMeta));
    } else if (birthdate.isRequired && isInserting) {
      context.missing(_birthdateMeta);
    }
    if (d.followers.present) {
      context.handle(_followersMeta,
          followers.isAcceptableValue(d.followers.value, _followersMeta));
    } else if (followers.isRequired && isInserting) {
      context.missing(_followersMeta);
    }
    if (d.following.present) {
      context.handle(_followingMeta,
          following.isAcceptableValue(d.following.value, _followingMeta));
    } else if (following.isRequired && isInserting) {
      context.missing(_followingMeta);
    }
    if (d.videos.present) {
      context.handle(
          _videosMeta, videos.isAcceptableValue(d.videos.value, _videosMeta));
    } else if (videos.isRequired && isInserting) {
      context.missing(_videosMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  User map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return User.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(UsersCompanion d) {
    final map = <String, Variable>{};
    if (d.uuid.present) {
      map['uuid'] = Variable<String, StringType>(d.uuid.value);
    }
    if (d.creationDate.present) {
      map['creation_date'] =
          Variable<DateTime, DateTimeType>(d.creationDate.value);
    }
    if (d.lastUpdate.present) {
      map['last_update'] = Variable<DateTime, DateTimeType>(d.lastUpdate.value);
    }
    if (d.username.present) {
      map['username'] = Variable<String, StringType>(d.username.value);
    }
    if (d.pseudonyme.present) {
      map['pseudonyme'] = Variable<String, StringType>(d.pseudonyme.value);
    }
    if (d.bio.present) {
      map['bio'] = Variable<String, StringType>(d.bio.value);
    }
    if (d.birthdate.present) {
      map['birthdate'] = Variable<DateTime, DateTimeType>(d.birthdate.value);
    }
    if (d.followers.present) {
      map['followers'] = Variable<int, IntType>(d.followers.value);
    }
    if (d.following.present) {
      map['following'] = Variable<int, IntType>(d.following.value);
    }
    if (d.videos.present) {
      map['videos'] = Variable<int, IntType>(d.videos.value);
    }
    return map;
  }

  @override
  $UsersTable createAlias(String alias) {
    return $UsersTable(_db, alias);
  }
}

class Tag extends DataClass implements Insertable<Tag> {
  final String uuid;
  final DateTime creationDate;
  final DateTime lastUpdate;
  final String tag;
  final int videos;
  Tag(
      {@required this.uuid,
      @required this.creationDate,
      @required this.lastUpdate,
      @required this.tag,
      @required this.videos});
  factory Tag.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    final intType = db.typeSystem.forDartType<int>();
    return Tag(
      uuid: stringType.mapFromDatabaseResponse(data['${effectivePrefix}uuid']),
      creationDate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}creation_date']),
      lastUpdate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}last_update']),
      tag: stringType.mapFromDatabaseResponse(data['${effectivePrefix}tag']),
      videos: intType.mapFromDatabaseResponse(data['${effectivePrefix}videos']),
    );
  }
  factory Tag.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Tag(
      uuid: serializer.fromJson<String>(json['uuid']),
      creationDate: serializer.fromJson<DateTime>(json['creationDate']),
      lastUpdate: serializer.fromJson<DateTime>(json['lastUpdate']),
      tag: serializer.fromJson<String>(json['tag']),
      videos: serializer.fromJson<int>(json['videos']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'uuid': serializer.toJson<String>(uuid),
      'creationDate': serializer.toJson<DateTime>(creationDate),
      'lastUpdate': serializer.toJson<DateTime>(lastUpdate),
      'tag': serializer.toJson<String>(tag),
      'videos': serializer.toJson<int>(videos),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Tag>>(bool nullToAbsent) {
    return TagsCompanion(
      uuid: uuid == null && nullToAbsent ? const Value.absent() : Value(uuid),
      creationDate: creationDate == null && nullToAbsent
          ? const Value.absent()
          : Value(creationDate),
      lastUpdate: lastUpdate == null && nullToAbsent
          ? const Value.absent()
          : Value(lastUpdate),
      tag: tag == null && nullToAbsent ? const Value.absent() : Value(tag),
      videos:
          videos == null && nullToAbsent ? const Value.absent() : Value(videos),
    ) as T;
  }

  Tag copyWith(
          {String uuid,
          DateTime creationDate,
          DateTime lastUpdate,
          String tag,
          int videos}) =>
      Tag(
        uuid: uuid ?? this.uuid,
        creationDate: creationDate ?? this.creationDate,
        lastUpdate: lastUpdate ?? this.lastUpdate,
        tag: tag ?? this.tag,
        videos: videos ?? this.videos,
      );
  @override
  String toString() {
    return (StringBuffer('Tag(')
          ..write('uuid: $uuid, ')
          ..write('creationDate: $creationDate, ')
          ..write('lastUpdate: $lastUpdate, ')
          ..write('tag: $tag, ')
          ..write('videos: $videos')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      uuid.hashCode,
      $mrjc(creationDate.hashCode,
          $mrjc(lastUpdate.hashCode, $mrjc(tag.hashCode, videos.hashCode)))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Tag &&
          other.uuid == uuid &&
          other.creationDate == creationDate &&
          other.lastUpdate == lastUpdate &&
          other.tag == tag &&
          other.videos == videos);
}

class TagsCompanion extends UpdateCompanion<Tag> {
  final Value<String> uuid;
  final Value<DateTime> creationDate;
  final Value<DateTime> lastUpdate;
  final Value<String> tag;
  final Value<int> videos;
  const TagsCompanion({
    this.uuid = const Value.absent(),
    this.creationDate = const Value.absent(),
    this.lastUpdate = const Value.absent(),
    this.tag = const Value.absent(),
    this.videos = const Value.absent(),
  });
  TagsCompanion copyWith(
      {Value<String> uuid,
      Value<DateTime> creationDate,
      Value<DateTime> lastUpdate,
      Value<String> tag,
      Value<int> videos}) {
    return TagsCompanion(
      uuid: uuid ?? this.uuid,
      creationDate: creationDate ?? this.creationDate,
      lastUpdate: lastUpdate ?? this.lastUpdate,
      tag: tag ?? this.tag,
      videos: videos ?? this.videos,
    );
  }
}

class $TagsTable extends Tags with TableInfo<$TagsTable, Tag> {
  final GeneratedDatabase _db;
  final String _alias;
  $TagsTable(this._db, [this._alias]);
  final VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  GeneratedTextColumn _uuid;
  @override
  GeneratedTextColumn get uuid => _uuid ??= _constructUuid();
  GeneratedTextColumn _constructUuid() {
    return GeneratedTextColumn(
      'uuid',
      $tableName,
      false,
    );
  }

  final VerificationMeta _creationDateMeta =
      const VerificationMeta('creationDate');
  GeneratedDateTimeColumn _creationDate;
  @override
  GeneratedDateTimeColumn get creationDate =>
      _creationDate ??= _constructCreationDate();
  GeneratedDateTimeColumn _constructCreationDate() {
    return GeneratedDateTimeColumn(
      'creation_date',
      $tableName,
      false,
    );
  }

  final VerificationMeta _lastUpdateMeta = const VerificationMeta('lastUpdate');
  GeneratedDateTimeColumn _lastUpdate;
  @override
  GeneratedDateTimeColumn get lastUpdate =>
      _lastUpdate ??= _constructLastUpdate();
  GeneratedDateTimeColumn _constructLastUpdate() {
    return GeneratedDateTimeColumn(
      'last_update',
      $tableName,
      false,
    );
  }

  final VerificationMeta _tagMeta = const VerificationMeta('tag');
  GeneratedTextColumn _tag;
  @override
  GeneratedTextColumn get tag => _tag ??= _constructTag();
  GeneratedTextColumn _constructTag() {
    return GeneratedTextColumn(
      'tag',
      $tableName,
      false,
    );
  }

  final VerificationMeta _videosMeta = const VerificationMeta('videos');
  GeneratedIntColumn _videos;
  @override
  GeneratedIntColumn get videos => _videos ??= _constructVideos();
  GeneratedIntColumn _constructVideos() {
    return GeneratedIntColumn(
      'videos',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [uuid, creationDate, lastUpdate, tag, videos];
  @override
  $TagsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'tags';
  @override
  final String actualTableName = 'tags';
  @override
  VerificationContext validateIntegrity(TagsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.uuid.present) {
      context.handle(
          _uuidMeta, uuid.isAcceptableValue(d.uuid.value, _uuidMeta));
    } else if (uuid.isRequired && isInserting) {
      context.missing(_uuidMeta);
    }
    if (d.creationDate.present) {
      context.handle(
          _creationDateMeta,
          creationDate.isAcceptableValue(
              d.creationDate.value, _creationDateMeta));
    } else if (creationDate.isRequired && isInserting) {
      context.missing(_creationDateMeta);
    }
    if (d.lastUpdate.present) {
      context.handle(_lastUpdateMeta,
          lastUpdate.isAcceptableValue(d.lastUpdate.value, _lastUpdateMeta));
    } else if (lastUpdate.isRequired && isInserting) {
      context.missing(_lastUpdateMeta);
    }
    if (d.tag.present) {
      context.handle(_tagMeta, tag.isAcceptableValue(d.tag.value, _tagMeta));
    } else if (tag.isRequired && isInserting) {
      context.missing(_tagMeta);
    }
    if (d.videos.present) {
      context.handle(
          _videosMeta, videos.isAcceptableValue(d.videos.value, _videosMeta));
    } else if (videos.isRequired && isInserting) {
      context.missing(_videosMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Tag map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Tag.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(TagsCompanion d) {
    final map = <String, Variable>{};
    if (d.uuid.present) {
      map['uuid'] = Variable<String, StringType>(d.uuid.value);
    }
    if (d.creationDate.present) {
      map['creation_date'] =
          Variable<DateTime, DateTimeType>(d.creationDate.value);
    }
    if (d.lastUpdate.present) {
      map['last_update'] = Variable<DateTime, DateTimeType>(d.lastUpdate.value);
    }
    if (d.tag.present) {
      map['tag'] = Variable<String, StringType>(d.tag.value);
    }
    if (d.videos.present) {
      map['videos'] = Variable<int, IntType>(d.videos.value);
    }
    return map;
  }

  @override
  $TagsTable createAlias(String alias) {
    return $TagsTable(_db, alias);
  }
}

class Commentaire extends DataClass implements Insertable<Commentaire> {
  final String uuid;
  final DateTime creationDate;
  final DateTime lastUpdate;
  final String message;
  final int likes;
  Commentaire(
      {@required this.uuid,
      @required this.creationDate,
      @required this.lastUpdate,
      @required this.message,
      @required this.likes});
  factory Commentaire.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    final intType = db.typeSystem.forDartType<int>();
    return Commentaire(
      uuid: stringType.mapFromDatabaseResponse(data['${effectivePrefix}uuid']),
      creationDate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}creation_date']),
      lastUpdate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}last_update']),
      message:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}message']),
      likes: intType.mapFromDatabaseResponse(data['${effectivePrefix}likes']),
    );
  }
  factory Commentaire.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Commentaire(
      uuid: serializer.fromJson<String>(json['uuid']),
      creationDate: serializer.fromJson<DateTime>(json['creationDate']),
      lastUpdate: serializer.fromJson<DateTime>(json['lastUpdate']),
      message: serializer.fromJson<String>(json['message']),
      likes: serializer.fromJson<int>(json['likes']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'uuid': serializer.toJson<String>(uuid),
      'creationDate': serializer.toJson<DateTime>(creationDate),
      'lastUpdate': serializer.toJson<DateTime>(lastUpdate),
      'message': serializer.toJson<String>(message),
      'likes': serializer.toJson<int>(likes),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<Commentaire>>(bool nullToAbsent) {
    return CommentairesCompanion(
      uuid: uuid == null && nullToAbsent ? const Value.absent() : Value(uuid),
      creationDate: creationDate == null && nullToAbsent
          ? const Value.absent()
          : Value(creationDate),
      lastUpdate: lastUpdate == null && nullToAbsent
          ? const Value.absent()
          : Value(lastUpdate),
      message: message == null && nullToAbsent
          ? const Value.absent()
          : Value(message),
      likes:
          likes == null && nullToAbsent ? const Value.absent() : Value(likes),
    ) as T;
  }

  Commentaire copyWith(
          {String uuid,
          DateTime creationDate,
          DateTime lastUpdate,
          String message,
          int likes}) =>
      Commentaire(
        uuid: uuid ?? this.uuid,
        creationDate: creationDate ?? this.creationDate,
        lastUpdate: lastUpdate ?? this.lastUpdate,
        message: message ?? this.message,
        likes: likes ?? this.likes,
      );
  @override
  String toString() {
    return (StringBuffer('Commentaire(')
          ..write('uuid: $uuid, ')
          ..write('creationDate: $creationDate, ')
          ..write('lastUpdate: $lastUpdate, ')
          ..write('message: $message, ')
          ..write('likes: $likes')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      uuid.hashCode,
      $mrjc(
          creationDate.hashCode,
          $mrjc(
              lastUpdate.hashCode, $mrjc(message.hashCode, likes.hashCode)))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Commentaire &&
          other.uuid == uuid &&
          other.creationDate == creationDate &&
          other.lastUpdate == lastUpdate &&
          other.message == message &&
          other.likes == likes);
}

class CommentairesCompanion extends UpdateCompanion<Commentaire> {
  final Value<String> uuid;
  final Value<DateTime> creationDate;
  final Value<DateTime> lastUpdate;
  final Value<String> message;
  final Value<int> likes;
  const CommentairesCompanion({
    this.uuid = const Value.absent(),
    this.creationDate = const Value.absent(),
    this.lastUpdate = const Value.absent(),
    this.message = const Value.absent(),
    this.likes = const Value.absent(),
  });
  CommentairesCompanion copyWith(
      {Value<String> uuid,
      Value<DateTime> creationDate,
      Value<DateTime> lastUpdate,
      Value<String> message,
      Value<int> likes}) {
    return CommentairesCompanion(
      uuid: uuid ?? this.uuid,
      creationDate: creationDate ?? this.creationDate,
      lastUpdate: lastUpdate ?? this.lastUpdate,
      message: message ?? this.message,
      likes: likes ?? this.likes,
    );
  }
}

class $CommentairesTable extends Commentaires
    with TableInfo<$CommentairesTable, Commentaire> {
  final GeneratedDatabase _db;
  final String _alias;
  $CommentairesTable(this._db, [this._alias]);
  final VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  GeneratedTextColumn _uuid;
  @override
  GeneratedTextColumn get uuid => _uuid ??= _constructUuid();
  GeneratedTextColumn _constructUuid() {
    return GeneratedTextColumn(
      'uuid',
      $tableName,
      false,
    );
  }

  final VerificationMeta _creationDateMeta =
      const VerificationMeta('creationDate');
  GeneratedDateTimeColumn _creationDate;
  @override
  GeneratedDateTimeColumn get creationDate =>
      _creationDate ??= _constructCreationDate();
  GeneratedDateTimeColumn _constructCreationDate() {
    return GeneratedDateTimeColumn(
      'creation_date',
      $tableName,
      false,
    );
  }

  final VerificationMeta _lastUpdateMeta = const VerificationMeta('lastUpdate');
  GeneratedDateTimeColumn _lastUpdate;
  @override
  GeneratedDateTimeColumn get lastUpdate =>
      _lastUpdate ??= _constructLastUpdate();
  GeneratedDateTimeColumn _constructLastUpdate() {
    return GeneratedDateTimeColumn(
      'last_update',
      $tableName,
      false,
    );
  }

  final VerificationMeta _messageMeta = const VerificationMeta('message');
  GeneratedTextColumn _message;
  @override
  GeneratedTextColumn get message => _message ??= _constructMessage();
  GeneratedTextColumn _constructMessage() {
    return GeneratedTextColumn(
      'message',
      $tableName,
      false,
    );
  }

  final VerificationMeta _likesMeta = const VerificationMeta('likes');
  GeneratedIntColumn _likes;
  @override
  GeneratedIntColumn get likes => _likes ??= _constructLikes();
  GeneratedIntColumn _constructLikes() {
    return GeneratedIntColumn(
      'likes',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [uuid, creationDate, lastUpdate, message, likes];
  @override
  $CommentairesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'commentaires';
  @override
  final String actualTableName = 'commentaires';
  @override
  VerificationContext validateIntegrity(CommentairesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.uuid.present) {
      context.handle(
          _uuidMeta, uuid.isAcceptableValue(d.uuid.value, _uuidMeta));
    } else if (uuid.isRequired && isInserting) {
      context.missing(_uuidMeta);
    }
    if (d.creationDate.present) {
      context.handle(
          _creationDateMeta,
          creationDate.isAcceptableValue(
              d.creationDate.value, _creationDateMeta));
    } else if (creationDate.isRequired && isInserting) {
      context.missing(_creationDateMeta);
    }
    if (d.lastUpdate.present) {
      context.handle(_lastUpdateMeta,
          lastUpdate.isAcceptableValue(d.lastUpdate.value, _lastUpdateMeta));
    } else if (lastUpdate.isRequired && isInserting) {
      context.missing(_lastUpdateMeta);
    }
    if (d.message.present) {
      context.handle(_messageMeta,
          message.isAcceptableValue(d.message.value, _messageMeta));
    } else if (message.isRequired && isInserting) {
      context.missing(_messageMeta);
    }
    if (d.likes.present) {
      context.handle(
          _likesMeta, likes.isAcceptableValue(d.likes.value, _likesMeta));
    } else if (likes.isRequired && isInserting) {
      context.missing(_likesMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Commentaire map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Commentaire.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(CommentairesCompanion d) {
    final map = <String, Variable>{};
    if (d.uuid.present) {
      map['uuid'] = Variable<String, StringType>(d.uuid.value);
    }
    if (d.creationDate.present) {
      map['creation_date'] =
          Variable<DateTime, DateTimeType>(d.creationDate.value);
    }
    if (d.lastUpdate.present) {
      map['last_update'] = Variable<DateTime, DateTimeType>(d.lastUpdate.value);
    }
    if (d.message.present) {
      map['message'] = Variable<String, StringType>(d.message.value);
    }
    if (d.likes.present) {
      map['likes'] = Variable<int, IntType>(d.likes.value);
    }
    return map;
  }

  @override
  $CommentairesTable createAlias(String alias) {
    return $CommentairesTable(_db, alias);
  }
}

abstract class _$Database extends GeneratedDatabase {
  _$Database(QueryExecutor e) : super(const SqlTypeSystem.withDefaults(), e);
  $VideosTable _videos;
  $VideosTable get videos => _videos ??= $VideosTable(this);
  $UsersTable _users;
  $UsersTable get users => _users ??= $UsersTable(this);
  $TagsTable _tags;
  $TagsTable get tags => _tags ??= $TagsTable(this);
  $CommentairesTable _commentaires;
  $CommentairesTable get commentaires =>
      _commentaires ??= $CommentairesTable(this);
  @override
  List<TableInfo> get allTables => [videos, users, tags, commentaires];
}
